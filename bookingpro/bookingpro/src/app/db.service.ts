//////////////////////////////////////
// db.service.ts
// @ Author Brian
////////////////////////////////////////

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DbService {
    dataUrl: string = "./assets/hbdata.json";

    constructor(private httpClient: HttpClient) { }

    getRooms(){
        //alert(this.dataUrl);
        return this.httpClient.get(this.dataUrl);
    }

    getBooked(){
        return this.httpClient.get(this.dataUrl);
    }

    getGuests(){
        return this.httpClient.get(this.dataUrl);
    }

    getCalendar(){
        return this.httpClient.get(this.dataUrl);
    }

    getBookingDate(){
        return new Date();
    }
}
