//////////////////////////////////////
// calendar.component.ts
// @ Author Brian
////////////////////////////////////////

import { Component, ChangeDetectionStrategy, ViewChild, ViewEncapsulation, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CalendarEvent, DAYS_OF_WEEK, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import { DbService } from '../db.service';

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    green: {
        primary: '#00ff00',
        secondary: '#0FFF9B'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class CalendarComponent implements OnInit {
    private calendar = [];
    private roomServ;
    view: string = 'month';
    viewDate: Date;
    events: CalendarEvent[] = [{
        start: new Date('2019-03-18'),
        end: new Date('2019-03-20'),
        title: 'Projest testing',
        color: colors.green,
        allDay: true
      },
      {
        start: new Date('2019-03-21'),
        end: new Date('2019-03-22'),
        title: 'Project debuging.',
        color: colors.yellow,
        allDay: true
      },
      {
        start: new Date(),
        end: new Date(),
        title: 'Bread & Breackfast 7:00AM - 10:30AM',
        color: colors.blue,
        allDay: true
      },
      {
        start: new Date(),
        end: new Date(),
        title: 'Lunch 12:00PM - 2:30PM',
        color: colors.yellow,
        allDay: true
      },
      {
        start: new Date(),
        end: new Date(),
        title: 'Super 5:00PM - 9:30PM',
        color: colors.green,
        allDay: true
      },
      {
        start: new Date("2019/03/28"),
        end: new Date("2019/03/28"),
        title: 'Booking for room #3',
        color: colors.red,
        allDay: true
      },
      {
        start: new Date("2019/3/29"),
        end: new Date("2019/3/30"),
        title: 'Booking for room #4',
        color: colors.blue,
        allDay: true
      },
      {
        start: new Date("2019/3/29"),
        end: new Date("2019/3/30"),
        title: 'Booking for room #5',
        color: colors.green,
        allDay: true
      },
      {
        start: new Date("2019/4/21"),
        end: new Date("2019/4/21"),
        title: 'Booking for room #9',
        color: colors.red,
        allDay: true
      },
      {
        start: new Date("2019/4/21"),
        end: new Date("2019/4/22"),
        title: 'Booking for room #10',
        color: colors.yellow,
        allDay: true
      },
      {
        start: new Date("2019/4/21"),
        end: new Date("2019/4/24"),
        title: 'Booking for room #11',
        color: colors.blue,
        allDay: true
      }
    ];
    weekStartsOn = DAYS_OF_WEEK.SUNDAY;
    constructor(private dataService: DbService) {
        this.viewDate = this.dataService.getBookingDate();
        this.roomServ = this.dataService.getCalendar();
        console.log("init0");
    }
    ngOnInit() {
        this.roomServ.subscribe(res =>{
            this.calendar = res["calendar"];
            this.localDB();
            console.log("init1");
        });
        console.log("init2");
    }
    bookEvents(){
        let i = 0;
        let l = this.calendar.length;
        let dat;
        for(i = 0; i < l;i++){
            dat = [{
                start: + new Date(this.calendar[i]["start"]),
                end: new Date(this.calendar[i]["end"]),
                title: this.calendar[i]["title"],
                color: this.eventColors(this.calendar[i]["color"]),
                allDay: this.eventAll(this.calendar[i]["allDay"])
            }];
            this.events.push(dat[0]);
        }
    }
    localDB(){
      let info = true;
      try{
          if(localStorage.getItem('calendar')){
              let colorBookings = JSON.parse(localStorage.getItem('calendar'));
              info = false;
              let i = 0;
              let l = colorBookings.length;
              let dat;
              for(i = 0; i < l;i++){
                  dat = [{
                      start: new Date(colorBookings[i]["start"]),
                      end: new Date(colorBookings[i]["end"]),
                      title: colorBookings[i]["title"],
                      color: this.eventColors(colorBookings[i]["color"]),
                      allDay: this.eventAll(colorBookings[i]["allDay"])
                  }];
                  this.events.push(dat[0]);
              }
          }
      }catch(e){
          console.log("calendarDBloc: "+e);
          info = true;
      }
      if(info){
        this.bookEvents();
      }
    }

    eventColors(eColor){
        if(eColor == "colors.red"){
            return colors.red;
        }
        if(eColor == "colors.green"){
            return colors.green;
        }
        if(eColor == "colors.blue"){
            return colors.blue;
        }
        return colors.yellow;
    }

    eventAll(alldEvent){
        if(alldEvent == "false"){
            return false;
        }
        return true;
    }
}
