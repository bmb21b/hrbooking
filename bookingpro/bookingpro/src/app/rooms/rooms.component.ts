//////////////////////////////////////
// rooms.component.ts
// @ Author Brian
////////////////////////////////////////

import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';

@Component({
    selector: 'app-rooms',
    templateUrl: './rooms.component.html',
    styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
    private booked = [];
    private rooms = [];
    private roomServ;
    private txbSearch;
    private roomInfo;
    private inpSearch;
    private inDate;
    private dbBookings = [];
    private availableR = true;
    private availableCl = " rgba(0,255,0,1)";

    constructor(private dataService: DbService) {
        this.txbSearch = this.dataService.getBookingDate();
        this.roomServ = this.dataService.getRooms();
        this.roomServ.subscribe(res =>{
            this.booked = res["booked"] as string [];
            this.rooms = res["rooms"] as string [];
        });
        this.localDB();
    }

    localDB(){
        try{
        if(localStorage.getItem('booked')){
            this.dbBookings = JSON.parse(localStorage.getItem('booked'));
        }
        }catch(e){
            console.log(e);
        }
    }

    ngOnInit() {
        this.txbSearch = new Date();
    }

    infoRoom(data,hr){
        //alert(data);
        this.inpSearch = new Date(this.txbSearch);
        let y = this.inpSearch.getFullYear();
        let m = this.inpSearch.getMonth() + 1;
        let d = this.inpSearch.getDate();
        this.inDate = y+"/"+m+"/"+d;
        if(!hr){
            this.roomInfo = "Room# "+data+": "+this.inDate+ ",  Available";
            //this.availableCl = "color: rgba(0,255,0,1)";
        }
        let i = 0;
        let l = this.booked.length;
        for(i = 0; i < l;i++){
          if(this.booked[i]["roomId"] == data){
              if(this.booked[i]["date"] == this.inDate){
                  console.log(this.roomInfo);
                  if(hr){
                      return false;
                  }
                  this.roomInfo = "Room# "+ data+": "+this.inDate+", Not Available. ";
                  //this.availableCl = "color: rgba(255,2,0,1)";
              }
          }
        }
        try{
          let y = this.dbBookings.length;
          for(i = 0; i < y;i++){
            if(this.dbBookings[i]["roomId"] == data){
                if(this.dbBookings[i]["date"] == this.inDate){
                      console.log(this.roomInfo);
                      if(hr){
                          return false;
                      }
                      this.roomInfo = "Room#@ "+ data+": "+this.inDate+", Not Available. ";
                      //this.availableCl = "color: rgba(255,25,0,1)";
                }
            }
          }
        }catch(e){
            console.log("InfoR: "+e);
        }
        if(hr){
            return true;
        }
    }

    available(data){
        let d = this.infoRoom(data, true);
        return(d);
    }

    infoC(data){
        return "blue";
    }

    bookNow(room,date,mes){
      alert(room+" "+date+" "+mes);
      let urLd = document.URL;
      let url = urLd.split("/rooms");
      alert(url[0]);
      //window.location = url[0]+"/book/" + "?bRoom="+room+",bDate="+this.txbSearch+",info=0";
    }

    txbSearchR(){
      let inpSearch = new Date(this.txbSearch);
      let y = inpSearch.getFullYear();
      let m = inpSearch.getMonth()+1;
      let d = inpSearch.getDate();
      let dt = ""+y+"-"+m+"-"+d+"";
      console.log("booking: "+dt);
      return dt;
    }
}
