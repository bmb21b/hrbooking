//////////////////////////////////////
// book.component.ts
// @ Author Brian
////////////////////////////////////////

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DbService } from '../db.service';

@Component({
    selector: 'app-book',
    templateUrl: './book.component.html',
    styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
    hbForm: FormGroup;
    contactName:  string  =  "";
    surname: string = "";
    email: string = "";
    bookDate;
    dateCap;
    room = 0;
    bookForm;
    submitted = false;
    success = false;
    available = false;
    bookDateA;
    nights;
    upDateG = true;
    private roomBookings = [];
    private dbBookings = [];
    private roomServ;
    private booked = [];
    private roomInfo;
    private tempBookedR = [];
    private inDate;
    updateC = 0;
    bookColor;
    colorBookings = [];
    startB;
    endB;
    updateCalendar = false;
    constructor(private formBuilder: FormBuilder, private dataService: DbService) {
        this.dateCap = this.dataService.getBookingDate();
        this.roomServ = this.dataService.getBooked();
        this.roomServ.subscribe(res =>{
            this.booked = res["booked"] as string [];
            this.colorBookings = res["calendar"] as string [];
            this.localDB();
        });
        this.bookNow();
    }

    ngOnInit() {
        this.hbForm = this.formBuilder.group({
            surnameN: ['', Validators.required],
            nameN: ['', Validators.required],
            emailN: ['', [Validators.required, Validators.pattern("[^ @]*@[^ @]*")]],
            roomN: ['', [Validators.required, Validators.min(1), Validators.max(12)]],
            nightsN: ['', [Validators.required, Validators.min(1), Validators.max(31)]],
            dateN: ['', Validators.required]
        });
    }

    localDB(){
        try{
        if(localStorage.getItem('booked')){
            this.dbBookings = JSON.parse(localStorage.getItem('booked'));
        }
        if(localStorage.getItem('guests')){
            this.roomBookings = JSON.parse(localStorage.getItem('guests'));
        }
        if(localStorage.getItem('calendar')){
            this.colorBookings = JSON.parse(localStorage.getItem('calendar'));
        }
        }catch(e){
            console.log(e);
        }
    }

    get fld(){ return this.hbForm.controls; }

    bookRoom(data){
        this.submitted = true;
        this.upDateG = true;
        this.updateCalendar = false;
        this.infoRoom(data);
        if(this.hbForm.invalid){
            this.success = false;
            return;
        }
        this.success = true;
        let rfDate = new Date();
        let ref = rfDate.getTime();
        let roomBooking = [{
            "id": ref,
            "name": this.contactName,
            "surname": this.surname,
            "email": this.email,
            "room": this.room,
            "nights": this.nights,
            "date": this.inDate,
            "dateCaptured": this.dateCap
        }];
        let dbBooking = [{
            "roomId": this.room,
            "date": this.inDate,
            "guestId": ref
        }];
        this.startB = this.inDate;
        this.endB = this.inDate;
        this.upDateDB(dbBooking, roomBooking);
        if(this.nights > 1){
            let x = 0;
            try{
            for(x = 1; x < this.nights; x++){
                let inpSearch = new Date(this.inDate);
                let y = inpSearch.getFullYear();
                let m = inpSearch.getMonth() + 1;
                let d = inpSearch.getDate() + 1;
                if(d > 31){
                    m++;
                    d = 1;
                }
                if(m > 12){
                    y++;
                    m = 1;
                }
                this.bookDate = new Date(y+"/"+m+"/"+d);
                this.available = false;
                this.infoRoom(data);
                dbBooking = [{
                    "roomId": this.room,
                    "date": this.inDate,
                    "guestId": ref
                }];

                if(this.available){
                    if(this.upDateG){
                        roomBooking = [{
                          "id": ref,
                          "name": this.contactName,
                          "surname": this.surname,
                          "email": this.email,
                          "room": this.room,
                          "nights": this.nights,
                          "date": this.inDate,
                          "dateCaptured": this.dateCap
                        }];
                        this.upDateDB(dbBooking, roomBooking);
                    }else
                    try{
                    this.dbBookings.push(dbBooking[0]);
                    this.endB = this.inDate;
                    localStorage.setItem('booked', JSON.stringify(this.dbBookings));
                    }catch(e){
                        console.log(e);
                    }
                }
            }
            }catch(e){
                console.log(e);
            }
        }
        this.colorCalendar(ref);
    }

    upDateDB(dbBooking, roomBooking){
        if(this.available){
            try{
            this.dbBookings.push(dbBooking[0]);
            this.roomBookings.push(roomBooking[0]);
            this.updateC++;
            this.updateCalendar = true;
            this.startB = this.inDate;
            this.endB = this.inDate;
            console.log("upDateDB..."+this.updateC);
            this.upDateG = false;
            localStorage.setItem('guests', JSON.stringify(this.roomBookings));
            localStorage.setItem('booked', JSON.stringify(this.dbBookings));
            }catch(e){
                console.log("upDateDB: "+e);
            }
        }
    }

    infoRoom(data){
        this.randColor();
        let inpSearch = new Date(this.bookDate);
        let y = inpSearch.getFullYear();
        let m = inpSearch.getMonth() + 1;
        let d = inpSearch.getDate();
        this.inDate = y+"/"+m+"/"+d;
        this.available = true;
        this.roomInfo = "Room# "+data.roomN+": "+this.inDate+ ",  Available";
        let i = 0;
        let l = this.booked.length;
        for(i = 0; i < l;i++){
            if(this.booked[i]["roomId"] == data.roomN){
                if(this.booked[i]["date"] == this.inDate){
                    this.roomInfo = "Room# "+ data.roomN+": "+this.inDate+", Not Available. ";
                    this.available = false;
                    console.log(this.roomInfo);
                    return;
                }
            }
        }
        this.infoLocal(data);
    }

    infoLocal(data){
        try{
        let i = 0;
        let l = this.dbBookings.length;
        for(i = 0; i < l;i++){
            if(this.dbBookings[i]["roomId"] == data.roomN){
                if(this.dbBookings[i]["date"] == this.inDate){
                    this.roomInfo = "Room# "+ data.roomN+": "+this.inDate+", Not Available. ";
                    this.available = false;
                    console.log(this.roomInfo);
                    return;
                }
            }
        }
        }catch(e){
            console.log("localDB: "+e);
        }
    }

    reset(){
        try{
        this.bookColor = this.randColor();
        console.log(this.bookColor);
        console.log("Reset...");
        this.dbBookings = [];
        this.roomBookings = [];
        this.colorBookings = [];
        localStorage.clear();
        }catch(e){
            console.log("Reset: "+e);
        }
    }

    randColor(){
      let rColors = ["colors.red", "colors.green", "colors.blue", "colors.yellow"];
      let x = Math.random();
      let y = Math.floor(x * 300);
      let z = rColors.length;
      x = y % z;
      return rColors[x];
    }

    colorCalendar(ref){
        let coBooking;
        if(this.updateCalendar){
            coBooking = [{
                "start": this.startB,
                "end": this.endB,
                "title": "Booking for room # "+this.room+", "+ref,
                "color": this.randColor(),
                "allDay": "true"
            }];
            try{
                this.updateCalendar = false;
                this.colorBookings.push(coBooking[0]);
                localStorage.setItem('calendar', JSON.stringify(this.colorBookings));
            }catch(e){
                console.log("Color Calender update:"+e )
            }
        }
    }

    bookNow(){
        let urLd = document.URL;
        let url1 = urLd.split(",");
        let url2 = url1.length;
        //alert(url2);
        try{
            if(url2 > 2){
              let url = urLd.split("bRoom");
              //alert(url[1]);
              let url3 = url[1].split(",");
              this.room = parseInt(url3[1]);
              //alert(url3[2]);
              let dateR = new Date(url3[2]);
              let d = dateR.getFullYear()+"/"+(dateR.getMonth()+1)+"/"+dateR.getDate();
              this.roomInfo = d;
              this.bookDate = new Date(dateR);
            }
        }catch(e){
            alert(e);
        }
    }
}
