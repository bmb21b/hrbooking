//////////////////////////////////////
// booked.component.ts
// @ Author Brian
////////////////////////////////////////

import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';

@Component({
    selector: 'app-booked',
    templateUrl: './booked.component.html',
    styleUrls: ['./booked.component.css']
})

export class BookedComponent implements OnInit {
    private booked = [];
    private guest = [];
    private roomServ;
    private guestInfo;
    private guestNumber;
    private guestCount = 0;
    private txbSearch;
    dbBookings = [];
    roomBookings = [];

    constructor(private dataService: DbService) {
        this.txbSearch = this.dataService.getBookingDate();
        this.roomServ = this.dataService.getRooms();
        this.roomServ.subscribe(res =>{
            this.booked = res["booked"] as string [];
            this.guest = res["guests"] as string [];
        });
        this.localDB();
    }

    localDB(){
        try{
        if(localStorage.getItem('booked')){
            this.dbBookings = JSON.parse(localStorage.getItem('booked'));
        }
        if(localStorage.getItem('guests')){
            this.roomBookings = JSON.parse(localStorage.getItem('guests'));
        }
        }catch(e){
            console.log(e);
        }
    }

    ngOnInit() {
    }

    public gCount(){
       return ++this.guestCount;
    }

    public info( gNumber ){
        this.guestCount = 0;
        let gn = gNumber["guestId"];
        let i = 0;
        let guest = this.guest;
        let l = guest.length;
        for( i = 0; i < l ; i++ ){
            if(gn == guest[i]["id"]){
                this.guestInfo = gn+" "+guest[i]["name"] +" "+guest[i]["room"]+" "+guest[i]["date"]+" "+guest[i]["nights"] ;
                break;
            }
        }
        let guestl = this.roomBookings;
        let loc = guestl.length;
        for( i = 0; i < loc ; i++ ){
            if(gn == guestl[i]["id"]){
                this.guestInfo ="@# "+ gn+" "+guestl[i]["name"] +" "+guestl[i]["room"]+" "+guestl[i]["date"]+" "+guestl[i]["nights"] ;
                break;
            }
        }
    }
}
