import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book/book.component';
import { RoomsComponent } from './rooms/rooms.component';
import { BookedComponent } from './booked/booked.component';
import { CalendarComponent } from './calendar/calendar.component';

const routes: Routes = [
    { path: "book", component: BookComponent },
    { path: "booked", component: BookedComponent },
    { path: "rooms", component: RoomsComponent },
    { path: "calendar", component: CalendarComponent },
    { path: "", component: RoomsComponent },
    { path: "book/:bRoom", component: BookComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
