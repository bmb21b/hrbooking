import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule, MatInputModule, MatNativeDateModule, MatButtonModule, MatCardModule, MatFormFieldModule  } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { RoomsComponent } from './rooms/rooms.component';
import { BookedComponent } from './booked/booked.component';
import { CalendarComponent } from './calendar/calendar.component';
import { BookComponent } from './book/book.component';

@NgModule({
    declarations: [
        AppComponent,
        NavComponent,
        RoomsComponent,
        BookedComponent,
        CalendarComponent,
        BookComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatDatepickerModule, MatInputModule, MatNativeDateModule, MatButtonModule, MatFormFieldModule,
        MatCardModule, FormsModule, ReactiveFormsModule,
        CommonModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
